import 'package:bwa_chatty/theme.dart';
import 'package:bwa_chatty/widgets/chat_text.dart';
import 'package:flutter/material.dart';
// import 'package:bwa_chatty/theme.dart';

class ChatPage extends StatefulWidget {
  final String imageUrl;
  final String name;

  ChatPage({this.imageUrl, this.name});
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffEBEFF3),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(20.0)),
                  color: Color(0xffFFFFFF),
                ),
                child: Row(
                  children: [
                    Image.asset(
                      widget.imageUrl,
                      width: 55,
                      height: 55,
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.name,
                          style: titleTextStyle,
                        ),
                        Text(
                          '14,209 Members',
                          style: subtitleTextStyle,
                        ),
                      ],
                    ),
                    Spacer(),
                    Image.asset(
                      'assets/images/call_btn.png',
                      width: 65,
                      height: 65,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              ChatText(
                foto: 'assets/images/friend1.png',
                text: 'How Are You guys ?',
                waktu: '2:30',
                posisi: 1,
              ),
              SizedBox(
                height: 30.0,
              ),
              ChatText(
                foto: 'assets/images/friend3.png',
                text: 'Find Here :P',
                waktu: '3:11',
                posisi: 1,
              ),
              SizedBox(
                height: 30.0,
              ),
              ChatText(
                foto: 'assets/images/friend4.png',
                text:
                    'Thinking about how to deal with this client from hell...',
                waktu: '3:11',
                posisi: 2,
              ),
              SizedBox(
                height: 30.0,
              ),
              ChatText(
                foto: 'assets/images/friend2.png',
                text: 'Love Them',
                waktu: '23:11',
                posisi: 1,
              ),
              SizedBox(
                height: 30.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
