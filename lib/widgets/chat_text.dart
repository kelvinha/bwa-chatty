import 'package:flutter/material.dart';
import 'package:bwa_chatty/theme.dart';

class ChatText extends StatelessWidget {
  final String foto;
  final String text;
  final String waktu;
  final int posisi;

  ChatText({this.foto, this.text, this.waktu, this.posisi});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 30.0, right: 30.0),
      child: posisi == 1 ? chatLeft() : chatRight(),
    );
  }

  Row chatLeft() {
    return Row(
      children: [
        Image.asset(
          foto,
          width: 65,
          height: 65,
        ),
        SizedBox(
          width: 12.0,
        ),
        Container(
          width: 200,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          decoration: BoxDecoration(
            color: Color(0xffE5E5E5),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                text,
                style: titleTextStyle.copyWith(
                  color: blackColor,
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                waktu,
                style: subtitleTextStyle,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Row chatRight() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          width: 250,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          decoration: BoxDecoration(
            color: Color(0xffFFFFFF),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
              bottomLeft: Radius.circular(20.0),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                text,
                style: titleTextStyle.copyWith(
                  color: blackColor,
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                waktu,
                style: subtitleTextStyle,
              ),
            ],
          ),
        ),
        SizedBox(
          width: 12.0,
        ),
        Image.asset(
          foto,
          width: 65,
          height: 65,
        ),
      ],
    );
  }
}
